import Cookies from "vue-cookies";
export default (to, from, next) => {
  if (
    localStorage.getItem("user") != null &&
    localStorage.getItem("user").length > 0
  ) {
    // verify with firebase or jwt
    let user = localStorage.getItem("user");
    user = JSON.parse(user);
    Cookies.set("email", user.uid);
    next();
  } else {
    localStorage.removeItem("user");
    Cookies.remove("email");
    next("/user/login");
  }
};
