import { Auth } from 'aws-amplify'
import Cookies from 'vue-cookies'

export default {
  state: {
    currentUser: localStorage.getItem('user') != null ? JSON.parse(localStorage.getItem('user')) : null,
    loginError: null,
    processing: false,
    registerError: null,
    registerSuccess: null
  },
  getters: {
    currentUser: state => state.currentUser,
    processing: state => state.processing,
    loginError: state => state.loginError,
    registerError: state => state.registerError,
    registerSuccess: state => state.registerSuccess

  },
  mutations: {
    setUser (state, payload) {
      console.log(payload)
      state.currentUser = payload
      state.processing = false
      state.loginError = null
    },
    setLogout (state) {
      state.currentUser = null
      state.processing = false
      state.loginError = null
    },
    setProcessing (state, payload) {
      state.processing = payload
      state.loginError = null
    },
    setError (state, payload) {
      state.loginError = payload
      state.currentUser = null
      state.processing = false
    },
    registerError (state, payload) {
      state.registerError = payload
      state.currentUser = null
      state.processing = false
    },
    registerSuccess (state, payload) {
      state.registerSuccess = payload
      state.currentUser = null
      state.processing = false
      state.registerError = null
    },
    clearError (state) {
      state.loginError = null
      state.registerError = null
    }
  },
  actions: {

    login ({ commit }, payload) {
      commit('clearError')
      commit('setProcessing', true)
      try {
        Auth.signIn(payload.email, payload.password).then(
          user => {
            console.log(user)
            const item = { uid: user.attributes.sub, title: user.attributes['custom:Name'], img: '/assets/img/profile-pic-l.jpg', id: user.attributes.sub, role: user.attributes['custom:role'] }
            console.log(item)
            localStorage.setItem('user', JSON.stringify(item))
            commit('setUser', item)
            console.log(user)
            Cookies.set('email', user.attributes.sub)
          },
          err => {
            localStorage.removeItem('user')
            commit('setError', err.message)
          }
        )
      } catch (error) {
        console.log(error)
        commit('setError', error)
      }
    },
    signOut ({ commit }) {
      Auth.signOut()
        .then(() => {
          localStorage.removeItem('user')
          commit('setLogout')
          Cookies.remove('email')
        }, _error => {
          console.log(_error)
        })
    },
    register ({ commit }, payload) {
      commit('clearError')
      commit('setProcessing', true)
      console.log('ocean', payload)

      let username = payload.email
      let password = payload.password
      let name = payload.firstName + ' ' + payload.lastName
      let role = payload.role

      try {
        const signupResponse = Auth.signUp({
          username,
          password,
          attributes: {
            email: username,
            'custom:Name': name,
            'custom:role': role
          }
        }).then(
          user => {
            commit('registerSuccess', 'You have recieved a confimation link on your email to verify.')
          },
          err => {
            console.log(err)
            commit('registerError', err.message)
          }
        )
        console.log(signupResponse)
        setTimeout(() => {
          // router.replace('/user/login')
          // router.go();
          // this.$router.push({ path: '/user/login' });
        }, 100)
      } catch (error) {
        console.log(error)
        commit('registerError', error)
      }
    },
    getUsers ({ commit }) {
    },
    async UserSubscriptionStatus ({ commit }, payload) {
      let status = payload.is_subscribed
      let subs_id = payload.subs_id

      let user = await Auth.currentAuthenticatedUser({ bypassCache: true }) // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
      console.log('users', user)
      const { attributes } = user
      console.log('attributes', attributes)
      console.log(' here now ')
      const userData = JSON.parse(JSON.stringify(attributes))
      console.log('userData')
      console.log(userData)
      console.log('subs_id', subs_id)
      console.log(typeof attributes['custom:subs_id'], typeof subs_id)
      if (status) {
        if (userData['custom:subs_id'] != undefined) {
          const subs_array = userData['custom:subs_id'].split(',')
          console.log(subs_array, subs_array.includes(subs_id))
          if (userData['custom:subs_id'].trim() != '' && subs_array.includes(subs_id) != true) {
            subs_id = userData['custom:subs_id'] + ',' + subs_id
          } else {
            if (userData['custom:subs_id'].trim() != '') {
              subs_id = userData['custom:subs_id']
            }
          }
        }
      } else {
        const subs_array = userData['custom:subs_id'].split(',')
        console.log(subs_array, subs_array.includes(subs_id))
        if (userData['custom:subs_id'].trim() != '' && subs_array.includes(subs_id) == true) {
          const index = subs_array.indexOf(subs_id)
          subs_array.splice(index, 1)
          console.log(subs_array)
          if (subs_array.length > 1) {
            subs_id = subs_array.join(',')
          } else if (subs_array.length == 1) {
            subs_id = subs_array[0]
          } else {
            subs_id = ' '
          }
        } else {
          subs_id = userData['custom:subs_id']
        }
      }
      console.log('subs_id', typeof subs_id)
      console.log('subs_id', subs_id)
      let result = await Auth.updateUserAttributes(user, {
        'custom:is_subscribed': status.toString(),
        'custom:subs_id': subs_id
      })
      console.log('ocean2',result)
    }
  }
}
