/*global chrome*/
import React, { Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
// import {Link} from 'react-router-dom';
// import $ from 'jquery';

export default class Index extends Component {
    constructor(props) {
        super(props);
        this.state = { 
          loginUrl: "http://localhost:8080",
          currentUrl: "",
          favIcon: "",
          title: "",
          baseUrl: "",
          inputTitle: "",
          inputNote: "",
          uid:"",
          isLogin: false,
          loader: false,
          bookmarkId: "",
          apiUrl: "https://6svp316h6i.execute-api.us-east-2.amazonaws.com/bookmark"
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleTitle = this.handleTitle.bind(this);
        this.handleNote = this.handleNote.bind(this);
    }
    handleSubmit(event) {
      event.preventDefault();
      this.setState({loader: true});
      var This = this;
      console.log(this.state);
      let config = {
        headers: {
          "content-type": "application/json",
          "Access-Control-Allow-Origin":"*"
        }
      }
      let id = "";
      if(this.state.bookmarkId){
        id = this.state.bookmarkId;
      }else{
        let d = new Date();
        id = d.valueOf();
        id = 'b'+id;
      }

      axios.put(this.state.apiUrl+'/bookmarks/',{
        "id":id,
        "bookmark_title":this.state.inputTitle,
        "bookmark_url":this.state.currentUrl,
        "bookmark_type":this.state.inputNote,
        "user_id":this.state.uid
      },config)
      .then(function (response) {
          console.log('new entry',response);
          This.setState({loader: false});
      })
      .catch(function (error) {
         console.log('new entry error',error);
         This.setState({loader: false});
      });
    }
    handleTitle(event){
      this.setState({inputTitle: event.target.value});
    }
    handleNote(event){
      this.setState({inputNote: event.target.value});
    }
    handleClick(event){
      window.close();
      chrome.tabs.update({url: 'https://voicelink.app', active: false});
    }
    handleClose(){
      window.close();
    }
    componentDidMount(){
      let config = {
        headers: {
          "content-type": "application/json",
          "Access-Control-Allow-Origin":"*"
        }
      }
      var This = this;
      chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
          console.log(tabs);
          var favIcon = tabs[0].favIconUrl;
          var title = tabs[0].title;
          var currentUrl = tabs[0].url;
          var baseUrl = currentUrl.split('//');
          var http = baseUrl[0];
          baseUrl = baseUrl[1].split('/')[0];
          baseUrl = http+"//"+baseUrl;

          This.setState({
            currentUrl,
            favIcon,
            title,
            baseUrl
          });
      });
      chrome.cookies.get({
        url: 'https://voicelink.app',
        name: 'email'
      }, function(res){
        This.setState({
          uid: res.value,
          isLogin: true,
          loader: true
        });
        axios.post(This.state.apiUrl+'/bookmarks/',{
          "bookmark_url":This.state.currentUrl,
          "user_id":This.state.uid
        },config)
        .then(function (response) {
            console.log('Get new entry',response);
            response = JSON.parse(response.data.body);
            if(response.Count > 0){
              This.setState({
                bookmarkId: response.Items[0].id, 
                inputTitle: response.Items[0].bookmark_title,
                inputNote: response.Items[0].bookmark_type
              });
            }
            console.log(response.Items)
            This.setState({loader: false});
        })
        .catch(function (error) {
           console.log('Get entry error',error);
           This.setState({loader: false});
        });
      });
    }
    render() {
      if(this.state.isLogin){
        if(this.state.loader){
          return (
            <div className="login-form" >
              <div className="loading"></div>
            </div>
          );
        }else{
          return (
            <div className="login-form" >
              <form onSubmit={this.handleSubmit}>		
                <div className="input-group" id="current-url"> 
                  <div className="title-bar" style={{width: "100%"}}>
                    <img src={this.state.favIcon} alt={this.state.title} style={{width: "61px"}} /> 
                    <label style={{width: "74%", marginLeft: "3%", overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap"}}>{this.state.title}</label>
                  </div> 
                  <div className="url-link" style={{marginLeft: "20%", marginTop: "-35px", color: "rgba(0,0,0,.54)", overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", width: "70%", fontSize: 13}}>
                    <a href={this.state.currentUrl} style={{color: "rgba(0,0,0,.54)"}}><span>{this.state.baseUrl}</span></a>
                  </div>
                  <input type="hidden" className="form-control" name="title" placeholder="Title" value={this.state.currentUrl}/>
                </div>
                <div className="form-group">
                  <input type="text" className="form-control" name="title" onChange={this.handleTitle} placeholder="Title" value={this.state.inputTitle}/>
                </div>  
                <div className="form-group">
                  <textarea type="text" className="form-control" name="note" onChange={this.handleNote} placeholder="Take a note">{this.state.inputNote}</textarea>
                </div>   
                <div className="form-group" style={{marginTop:30}}>
                  <button type="button" onClick={this.handleClose} style={{width: "20%"}} className="btn btn-success btn-block login-btn">Cancel</button>
                  <button type="submit" style={{width: "20%"}} className="btn btn-success btn-block login-btn right">Save</button>
                </div>  
              </form>
            </div>
          );
        }
      }else{
        return (
          <div className="login-form" style={{textAlign: "center", padding: 50, backgroundColor: '#0000007a', color: '#fff'}} >
            <span style={{display: "table-header-group"}}>You must sign in to use the VoiceLink Chrome Extension.</span>
            <span style={{display: "inline-block", textAlign: "center", width: "100%", marginTop: 10}}><Link to="#" className="white-anchor" onClick={this.handleClick}>Sign in</Link></span>
          </div>
        );
      }
    }
  }